package com.gmail.val59000mc.spigotutils.items;


import com.google.common.collect.Sets;
import org.bukkit.ChatColor;
import org.bukkit.Material;

import java.util.Collections;
import java.util.Set;

import static org.bukkit.Material.*;

public class ItemUtils {

    public static Set<Material> WOOL_MATERIALS = Collections.unmodifiableSet(Sets.newHashSet(
        LEGACY_WOOL,
        WHITE_WOOL,
        BLACK_WOOL,
        BLUE_WOOL,
        BROWN_WOOL,
        CYAN_WOOL,
        GRAY_WOOL,
        GREEN_WOOL,
        LIGHT_BLUE_WOOL,
        LIGHT_GRAY_WOOL,
        LIME_WOOL,
        MAGENTA_WOOL,
        ORANGE_WOOL,
        PINK_WOOL,
        PURPLE_WOOL,
        RED_WOOL,
        YELLOW_WOOL
    ));

    private ItemUtils() {
        throw new IllegalArgumentException("This is an utility class");
    }

    public static boolean isWool(Material material) {
        if (material == null) {
            return false;
        }
        switch (material) {
            case LEGACY_WOOL:
            case WHITE_WOOL:
            case BLACK_WOOL:
            case BLUE_WOOL:
            case BROWN_WOOL:
            case CYAN_WOOL:
            case GRAY_WOOL:
            case GREEN_WOOL:
            case LIGHT_BLUE_WOOL:
            case LIGHT_GRAY_WOOL:
            case LIME_WOOL:
            case MAGENTA_WOOL:
            case ORANGE_WOOL:
            case PINK_WOOL:
            case PURPLE_WOOL:
            case RED_WOOL:
            case YELLOW_WOOL:
                return true;
            default:
                return false;
        }
    }

    public static boolean isColoredTerracotta(Material material) {
        if (material == null) {
            return false;
        }
        switch (material) {
            case BLACK_TERRACOTTA:
            case BLUE_TERRACOTTA:
            case BROWN_TERRACOTTA:
            case CYAN_TERRACOTTA:
            case GRAY_TERRACOTTA:
            case GREEN_TERRACOTTA:
            case LIGHT_BLUE_TERRACOTTA:
            case LIGHT_GRAY_TERRACOTTA:
            case LIME_TERRACOTTA:
            case MAGENTA_TERRACOTTA:
            case ORANGE_TERRACOTTA:
            case PINK_TERRACOTTA:
            case PURPLE_TERRACOTTA:
            case RED_TERRACOTTA:
            case WHITE_TERRACOTTA:
            case YELLOW_TERRACOTTA:
                return true;
            default:
                return false;
        }
    }


    public static boolean isGlazedTerracotta(Material material) {
        if (material == null) {
            return false;
        }
        switch (material) {
            case BLACK_GLAZED_TERRACOTTA:
            case BLUE_GLAZED_TERRACOTTA:
            case BROWN_GLAZED_TERRACOTTA:
            case CYAN_GLAZED_TERRACOTTA:
            case GRAY_GLAZED_TERRACOTTA:
            case GREEN_GLAZED_TERRACOTTA:
            case LIGHT_BLUE_GLAZED_TERRACOTTA:
            case LIGHT_GRAY_GLAZED_TERRACOTTA:
            case LIME_GLAZED_TERRACOTTA:
            case MAGENTA_GLAZED_TERRACOTTA:
            case ORANGE_GLAZED_TERRACOTTA:
            case PINK_GLAZED_TERRACOTTA:
            case PURPLE_GLAZED_TERRACOTTA:
            case RED_GLAZED_TERRACOTTA:
            case WHITE_GLAZED_TERRACOTTA:
            case YELLOW_GLAZED_TERRACOTTA:
                return true;
            default:
                return false;
        }
    }

    public static boolean isTerracotta(Material material) {
        if (material == null) {
            return false;
        }
        switch (material) {
            case TERRACOTTA:
            case BLACK_GLAZED_TERRACOTTA:
            case BLACK_TERRACOTTA:
            case BLUE_GLAZED_TERRACOTTA:
            case BLUE_TERRACOTTA:
            case BROWN_GLAZED_TERRACOTTA:
            case BROWN_TERRACOTTA:
            case CYAN_GLAZED_TERRACOTTA:
            case CYAN_TERRACOTTA:
            case GRAY_GLAZED_TERRACOTTA:
            case GRAY_TERRACOTTA:
            case GREEN_GLAZED_TERRACOTTA:
            case GREEN_TERRACOTTA:
            case LIGHT_BLUE_GLAZED_TERRACOTTA:
            case LIGHT_BLUE_TERRACOTTA:
            case LIGHT_GRAY_GLAZED_TERRACOTTA:
            case LIGHT_GRAY_TERRACOTTA:
            case LIME_GLAZED_TERRACOTTA:
            case LIME_TERRACOTTA:
            case MAGENTA_GLAZED_TERRACOTTA:
            case MAGENTA_TERRACOTTA:
            case ORANGE_GLAZED_TERRACOTTA:
            case ORANGE_TERRACOTTA:
            case PINK_GLAZED_TERRACOTTA:
            case PINK_TERRACOTTA:
            case PURPLE_GLAZED_TERRACOTTA:
            case PURPLE_TERRACOTTA:
            case RED_GLAZED_TERRACOTTA:
            case RED_TERRACOTTA:
            case WHITE_GLAZED_TERRACOTTA:
            case WHITE_TERRACOTTA:
            case YELLOW_GLAZED_TERRACOTTA:
            case YELLOW_TERRACOTTA:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWoodenPressurePlate(Material material) {
        if (material == null) {
            return false;
        }
        switch (material) {
            case ACACIA_PRESSURE_PLATE:
            case BIRCH_PRESSURE_PLATE:
            case DARK_OAK_PRESSURE_PLATE:
            case JUNGLE_PRESSURE_PLATE:
            case OAK_PRESSURE_PLATE:
            case SPRUCE_PRESSURE_PLATE:
                return true;
            default:
                return false;
        }
    }

    public static boolean isBanner(Material material) {
        if (material == null) {
            return false;
        }
        switch (material) {
            case LEGACY_BANNER:
            case LEGACY_STANDING_BANNER:
            case LEGACY_WALL_BANNER:
            case BLACK_BANNER:
            case BLACK_WALL_BANNER:
            case BLUE_BANNER:
            case BLUE_WALL_BANNER:
            case BROWN_BANNER:
            case BROWN_WALL_BANNER:
            case CYAN_BANNER:
            case CYAN_WALL_BANNER:
            case GRAY_BANNER:
            case GRAY_WALL_BANNER:
            case GREEN_BANNER:
            case GREEN_WALL_BANNER:
            case LIGHT_BLUE_BANNER:
            case LIGHT_BLUE_WALL_BANNER:
            case LIGHT_GRAY_BANNER:
            case LIGHT_GRAY_WALL_BANNER:
            case LIME_BANNER:
            case LIME_WALL_BANNER:
            case MAGENTA_BANNER:
            case MAGENTA_WALL_BANNER:
            case ORANGE_BANNER:
            case ORANGE_WALL_BANNER:
            case PINK_BANNER:
            case PINK_WALL_BANNER:
            case PURPLE_BANNER:
            case PURPLE_WALL_BANNER:
            case RED_BANNER:
            case RED_WALL_BANNER:
            case WHITE_BANNER:
            case WHITE_WALL_BANNER:
            case YELLOW_BANNER:
            case YELLOW_WALL_BANNER:
                return true;
            default:
                return false;
        }
    }

    public static Material toWoolMaterial(ChatColor color) {
        if (color == null) {
            return null;
        }
        switch (color) {
            case AQUA:
                return CYAN_WOOL;
            case BLACK:
                return BLACK_WOOL;
            case BLUE:
            case DARK_BLUE:
                return BLUE_WOOL;
            case DARK_AQUA:
            case DARK_PURPLE:
                return PURPLE_WOOL;
            case GRAY:
            case DARK_GRAY:
                return GRAY_WOOL;
            case GREEN:
            case DARK_GREEN:
                return GREEN_WOOL;
            case RED:
            case DARK_RED:
                return RED_WOOL;
            case GOLD:
                return ORANGE_WOOL;
            case LIGHT_PURPLE:
                return PINK_WOOL;
            case YELLOW:
                return YELLOW_WOOL;
            default:
            case WHITE:
            case MAGIC:
            case STRIKETHROUGH:
            case UNDERLINE:
            case RESET:
            case ITALIC:
            case BOLD:
                return WHITE_WOOL;
        }
    }

    public static Material toBannerMaterial(ChatColor color) {
        if (color == null) {
            return null;
        }
        switch (color) {
            case AQUA:
                return CYAN_BANNER;
            case BLACK:
                return BLACK_BANNER;
            case BLUE:
            case DARK_BLUE:
                return BLUE_BANNER;
            case DARK_AQUA:
            case DARK_PURPLE:
                return PURPLE_BANNER;
            case GRAY:
            case DARK_GRAY:
                return GRAY_BANNER;
            case GREEN:
            case DARK_GREEN:
                return GREEN_BANNER;
            case RED:
            case DARK_RED:
                return RED_BANNER;
            case GOLD:
                return ORANGE_BANNER;
            case LIGHT_PURPLE:
                return PINK_BANNER;
            case YELLOW:
                return YELLOW_BANNER;
            default:
            case WHITE:
            case MAGIC:
            case STRIKETHROUGH:
            case UNDERLINE:
            case RESET:
            case ITALIC:
            case BOLD:
                return WHITE_BANNER;
        }
    }

    public static Material toWallBannerMaterial(ChatColor color) {
        if (color == null) {
            return null;
        }
        switch (color) {
            case AQUA:
                return CYAN_WALL_BANNER;
            case BLACK:
                return BLACK_WALL_BANNER;
            case BLUE:
            case DARK_BLUE:
                return BLUE_WALL_BANNER;
            case DARK_AQUA:
            case DARK_PURPLE:
                return PURPLE_WALL_BANNER;
            case GRAY:
            case DARK_GRAY:
                return GRAY_WALL_BANNER;
            case GREEN:
            case DARK_GREEN:
                return GREEN_WALL_BANNER;
            case RED:
            case DARK_RED:
                return RED_WALL_BANNER;
            case GOLD:
                return ORANGE_WALL_BANNER;
            case LIGHT_PURPLE:
                return PINK_WALL_BANNER;
            case YELLOW:
                return YELLOW_WALL_BANNER;
            default:
            case WHITE:
            case MAGIC:
            case STRIKETHROUGH:
            case UNDERLINE:
            case RESET:
            case ITALIC:
            case BOLD:
                return WHITE_WALL_BANNER;
        }
    }
}
