package com.gmail.val59000mc.spigotutils.items;

import com.google.common.collect.Lists;
import org.bukkit.potion.PotionType;

import java.util.*;

import static org.bukkit.potion.PotionType.*;

public enum PotionTypes {
    POSITIVE_EFFECTS(
        NIGHT_VISION,
        INVISIBILITY,
        JUMP,
        FIRE_RESISTANCE,
        SPEED,
        WATER_BREATHING,
        INSTANT_HEAL,
        REGEN,
        STRENGTH,
        LUCK,
        TURTLE_MASTER,
        SLOW_FALLING
    ),

    NEGATIVE_EFFECTS(

        SLOWNESS,
        INSTANT_DAMAGE,
        POISON,
        WEAKNESS
    ),

    NO_EFFECTS(
        UNCRAFTABLE,
        WATER,
        MUNDANE,
        THICK,
        AWKWARD
    );

    private static final Map<PotionType, PotionTypes> REVERSE_CLASSIFICATION;

    static {
        Map<PotionType, PotionTypes> reverseClassification = new HashMap<>();
        Arrays.stream(PotionTypes.values())
            .forEach(category -> category.getEffectTypes()
                .forEach(type -> {
                    reverseClassification.put(type, category);
                }));
        REVERSE_CLASSIFICATION = Collections.unmodifiableMap(reverseClassification);
    }

    private final List<PotionType> effectTypes;

    PotionTypes(PotionType... effectTypes) {
        if (effectTypes == null) {
            this.effectTypes = Collections.emptyList();
        } else {
            this.effectTypes = Collections.unmodifiableList(Lists.newArrayList(effectTypes));
        }
    }

    public List<PotionType> getEffectTypes() {
        return effectTypes;
    }

    public static boolean isPositiveEffect(PotionType potionType) {
        return POSITIVE_EFFECTS.equals(REVERSE_CLASSIFICATION.get(potionType));
    }

    public static boolean isNegativeEffect(PotionType potionType) {
        return NEGATIVE_EFFECTS.equals(REVERSE_CLASSIFICATION.get(potionType));
    }

    public static boolean isNoEffect(PotionType potionType) {
        return NO_EFFECTS.equals(REVERSE_CLASSIFICATION.get(potionType));
    }
}
