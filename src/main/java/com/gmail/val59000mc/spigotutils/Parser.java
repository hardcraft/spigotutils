package com.gmail.val59000mc.spigotutils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;


public class Parser {

    public static <E extends Enum<E>> Enum<E> parseEnum(Class<E> enumClass, String toParse, Enum<E> defaultValue, String errorLog) {

        Enum<E> e;
        try {
            e = Enum.valueOf(enumClass, toParse.toUpperCase());
        } catch (IllegalArgumentException ex) {
            Logger.warnC(errorLog);
            e = defaultValue;
        }

        return e;
    }


    public static Location parseLocation(String toParse) {
        try {
            String[] arr = toParse.split(" ");
            World world = Bukkit.getWorld(arr[0]);
            if (world == null)
                world = Bukkit.getWorlds().get(0);
            return new Location(
                world,
                Double.parseDouble(arr[1]),
                Double.parseDouble(arr[2]),
                Double.parseDouble(arr[3]),
                Float.parseFloat(arr[4]),
                Float.parseFloat(arr[5])
            );
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Could parse location : %s", toParse), e);
        }
    }

    public static Vector parseVector(String toParse) {
        try {
            String[] arr = toParse.split(" ");
            return new Vector(
                Double.parseDouble(arr[0]),
                Double.parseDouble(arr[1]),
                Double.parseDouble(arr[2])
            );
        } catch (Exception e) {
            Logger.warnC(e.getMessage());
            throw new IllegalArgumentException(String.format("Could parse vector : %s", toParse));
        }
    }

    public static Location parseLocation(World world, String toParse) {
        try {
            String[] arr = toParse.split(" ");
            if (world == null)
                world = Bukkit.getWorlds().get(0);
            return new Location(
                world,
                Double.parseDouble(arr[0]),
                Double.parseDouble(arr[1]),
                Double.parseDouble(arr[2]),
                Float.parseFloat(arr[3]),
                Float.parseFloat(arr[4])
            );
        } catch (Exception e) {
            Logger.warnC("Couldn't parse location, error: " + e.getMessage() + ", defaulting to world's spawn location");
            throw new IllegalArgumentException(String.format("Could parse location : %s", toParse), e);
        }
    }

}
