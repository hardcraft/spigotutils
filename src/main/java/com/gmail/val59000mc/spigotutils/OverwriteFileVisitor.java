package com.gmail.val59000mc.spigotutils;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.CONTINUE;

public class OverwriteFileVisitor extends SimpleFileVisitor<Path> {

    private final Path source;
    private final Path target;

    public OverwriteFileVisitor(Path source, Path target) {
        this.source = source;
        this.target = target;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        return doCopy(dir);
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        return doCopy(file);
    }

    private FileVisitResult doCopy(Path visitedSource) {
        Path newFile = target.resolve(source.relativize(visitedSource));

        try {
            Files.copy(visitedSource, newFile);
        } catch (IOException ioException) {
            //log it and move
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        return CONTINUE;
    }
}
