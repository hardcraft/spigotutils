## Commands

### Install jar in local maven repository

mvn install:install-file -Dfile=spigot-1.14.4-R0.1-SNAPSHOT.jar -DpomFile=../pom.xml

E.g. : 

```

mvn install:install-file -Dfile=MythicMobs-4.6.5.jar -DpomFile=pom.xml
mvn install:install-file -Dfile=MCCore.jar -DgroupId=com.sucy.mc -DartifactId=McCore -Dpackaging=jar -Dversion=2.0.0-SNAPSHOT
mvn install:install-file -Dfile=SkillAPI.jar -DgroupId=com.sucy.mc -DartifactId=SkillAPI -Dpackaging=jar -Dversion=2.0.0-SNAPSHOT

```
